<%@ page import="java.util.List" %>
<%@ page import="com.aaamouu.mvc.model.Employee" %><%--
  Created by IntelliJ IDEA.
  User: yaakar
  Date: 21/06/2024
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee Record</title>
</head>
<body>
<%List<Employee> employees = (List<Employee>) request.getAttribute("employees");%>

<table border="1" style="width: 50%" height="50%">
  <thead>
  <tr>
    <th>ID</th>
    <th>First Name</th>
    <th>Last Name</th>
  </tr>
  </thead>
  <tbody>
  <!--   for (Todo todo: todos) {  -->
  <% for(Employee employee : employees){ %>
  <tr>
    <td><%=employee.getId()%></td>
    <td><%=employee.getFirstName()%></td>
    <td><%=employee.getLastName()%></td>
  </tr>
  <%} %>
  </tbody>

</table>
</body>
</html>
