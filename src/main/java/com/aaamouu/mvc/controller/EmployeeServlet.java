package com.aaamouu.mvc.controller;

import com.aaamouu.mvc.model.EmployeeService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "EmployeeServlet", value = "/EmployeeRecord")
public class EmployeeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private EmployeeService employeeService;

    public EmployeeServlet(){
        //super();
        this.employeeService = new EmployeeService();
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("employees", this.employeeService.getEmployee());

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/employees.jsp");
        dispatcher.forward(request, response);
    }
}