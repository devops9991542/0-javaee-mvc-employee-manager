package com.aaamouu.mvc.model;

import java.util.Arrays;
import java.util.List;

public class EmployeeService {
    // A method that will return a list of employees
    public List<Employee> getEmployee(){
        return Arrays.asList(new Employee(100, "Ibou", "Faye"),
                new Employee(101, "Fallou", "Diakhate"),
                new Employee(102, "Leo", "Messi"));
    }
}
